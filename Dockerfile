FROM nginx
MAINTAINER Stefane Fivaz
ENV PORT=80

COPY . /usr/share/nginx/html
EXPOSE $PORT
